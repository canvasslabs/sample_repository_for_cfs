# README #

### What is this repository for? ###

* Sample project containing a sample Bitbucket for Workflows workflow.
* This sample shows how to incorporate Vulnerability analysis into your current workflow.
* Version 1.0

### How do I get set up? ###

* See file: Getting Started w/Canvass For Security and Bitbucket Pipelines v1.9

### Who do I talk to? ###

* Send questions or comments to charlie@canvasslabs.com